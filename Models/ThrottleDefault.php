<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ThrottleDefault extends Model
{
	/**
	 * Indicates if the model should be timestamped.
	 *
	 * @var bool
	 */
	public $timestamps = true;
	public $guarded = ['id'];

	public function affiliate() {
		return $this->belongsTo('App\Models\Affiliate');
	}

	public function offer() {
		return $this->belongsTo('App\Models\Offer');
	}
}
