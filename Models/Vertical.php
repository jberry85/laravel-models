<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vertical extends Model {
	use SoftDeletes;

	protected $guarded = ['id', 'created_at', 'updated_at', 'deleted_at'];

	// Cast payable_criteria JSON to array
	protected $casts = ['post_criteria' => 'array'];

	public function offerConfigs() {
		return $this->hasManyThrough(OfferConfig::class, OfferConfigVertical::class);
	}

	public function offerConfigVerticals() {
		return $this->hasMany(OfferConfigVertical::class);
	}
}
