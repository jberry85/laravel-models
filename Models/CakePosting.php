<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CakePosting extends Model
{
	public function signup() {
		return $this->belongsTo('App\Models\Signup', 'signup_id');
	}

	public $timestamps = false;
}
