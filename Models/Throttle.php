<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Throttle extends Model {
	protected $table = 'throttles';
	
	
	/**
	 * Indicates if the model should be timestamped.
	 *
	 * @var bool
	 */
	public $timestamps = FALSE;
	/* deny mass assignment to these */
	protected $guarded = ['id', 'date_occurred'];

	protected $dates = [
		'date_occurred'
	];
}
