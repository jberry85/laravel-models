<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OfferSplit extends Model {
	/**
	 * Indicates if the model should be timestamped.
	 *
	 * @var bool
	 */
	public $timestamps = true;
	public $guarded = ['id', 'created_at', 'updated_at'];

	public function rootOffer() {
		return $this->belongsTo('App\Models\Offer', 'root_offer_id', 'id');
	}

	public function offer() {
		return $this->belongsTo('App\Models\Offer', 'id', 'id');
	}
}
