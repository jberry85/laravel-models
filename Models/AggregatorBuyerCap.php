<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AggregatorBuyerCap extends Model {
    protected $table = 'aggregator_buyer_caps';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = TRUE;

    /* deny mass assignment to these */
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function buyer() {
        return $this->belongsTo(AggregatorBuyer::class, 'buyer_id');
    }
}
