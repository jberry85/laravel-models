<?php

namespace App\Models;

use App\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;

class AggregatorTransaction extends Model {
	use Translatable;
	protected $table = 'aggregator_transactions';
	
	// use Translatable trait to automatically translate json
	public $translatable = ['json_data'];
	public $casts = ['json_data' => 'json'];
	/**
	 * Indicates if the model should be timestamped.
	 *
	 * @var bool
	 */
	public $timestamps = TRUE;
	
	/* deny mass assignment to these */
	protected $guarded = ['id','created_at', 'updated_at'];
	
	public function setUpdatedAt($value) {
		//Do-nothing
	}
	
	public function getUpdatedAtColumn() {
		//Do-nothing
		return NULL;
	}
}
