<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model {
	/**
	 * Indicates if the model should be timestamped.
	 *
	 * @var bool
	 */
	public $timestamps = FALSE;
	public $guarded = ['id'];
	public $hidden = ['pivot'];

    /* one-to-many relationship to contracts */
    public function contracts() {
        return $this->hasMany(Contract::class);
    }

    public function affiliates() {
        return $this->belongsToMany(Affiliate::class, 'campaigns')->withPivot('id');
    }

    /* one-to-one relationship to contracts via default contract id */
    public function default_contract() {
        return $this->hasOne(Contract::class);
    }

    /* one-to-many targets */
    public function targets() {
        return $this->hasMany(Target::class);
    }

	public function offerSplit() {
		return $this->hasOne(OfferSplit::class, 'id', 'id');
	}

	public function childOffers() {
		return $this->belongsToMany(Offer::class, 'offer_splits', 'root_offer_id', 'id');
	}

	public function throttleTargets() {
		return $this->hasMany(ThrottleTarget::class);
	}

	public function throttleDefaults() {
		return $this->hasMany(ThrottleDefault::class);
	}

	public function throttleDecisions() {
		return $this->hasMany(ThrottleDecision::class);
	}
}
