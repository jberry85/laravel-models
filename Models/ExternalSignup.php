<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExternalSignup extends Model {
    protected $table = 'external_signups';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = FALSE;

	/* deny mass assignment to these */
	protected $guarded = array('id');

	/**
	 * @return string
	 */
	public function getFullNameAttribute() {
        return $this->first_name . ' ' . $this->last_name;
    }

	/**
	 * @return bool|string
	 */
	public function getEmailDomainAttribute() {
		if (filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
			return substr(strrchr($this->email, "@"), 1);
		} else {
			return '';
		}
	}

	/**
	 * @return mixed
	 */
	public function getHomePhoneAttribute() {
		return isset($this->phone_primary) ? $this->phone_primary : $this->phone_home;
	}
}
