<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OfferConfigVertical extends Model {
	protected $guarded = ['id', 'created_at', 'updated_at'];

	public function offerConfig() {
		return $this->belongsTo(OfferConfig::class);
	}

	public function vertical() {
		return $this->belongsTo(Vertical::class);
	}
}
